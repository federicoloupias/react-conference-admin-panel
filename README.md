# Conference Admin Panel

An [MVP](https://en.wikipedia.org/wiki/Minimum_viable_product) of a conference admin panel.

This task uses **React Hooks**, **React Router** and **React Testing Library (`@testing-library/react`)**.

## About App

Simple conference admin panel application to enable the conference Committee Members to manage the `Call for Papers` process

### Fake Backend

There is no real backend behind the app. When starting the application (`npm start`), the app will automatically register a fake backend API defined in `src/api/fakeHttpApi.js`. In order to make HTTP requests, methods from `src/api/httpApi.js` should be called. HTTP requests have random latency simulated – you can expect them to take from 0 to 2 seconds.

In tests, all requests are mocked using `axios-mock-adapter`.

## Setup

Follow these steps for correct application setup:

1. `npm install` – install dependencies
2. `npm test` – run all tests
3. `npm start` – serve the app at [http://localhost:3000/](http://localhost:3000/) (it automatically opens the app in your default browser)
