import React from "react";
import { Link } from "react-router-dom";
import ProposalRow from "../ProposalRow";

import "./ProposalList.css";

export const ProposalList = ({ proposals, onProposalStatusUpdate }) => {
 return (
    <ul data-testid="proposal-list" className="ProposalList">
        {proposals.map((proposal) => (
            <li
                key={proposal.id}
                className="ProposalList__item"
            >
                <Link
                    key={proposal.id}
                    className="ProposalList__item__link"
                    to={{
                        pathname: `/proposals/${proposal.id}`,
                        state:{ ...proposal }
                    }}
                >
                    <ProposalRow
                        proposal={ proposal }
                        onStatusUpdate={onProposalStatusUpdate}
                    />
                </Link>
            </li>
        ))}
    </ul>
    )
}

export default ProposalList;
