import React from "react";

import DetailsSection from "../DetailsSection";

import "./ProposalDetails.css";

const ProposalDetails = ({ talk }) => {
    const { speaker, description, category } = talk;
    return (
        <div data-testid="proposal-details" className="ProposalDetails">
            <DetailsSection
                className="ProposalDetails__speaker"
                name="Speaker"
                content={speaker}
            >
            </DetailsSection>
            <DetailsSection
                className="ProposalDetails__category"
                name="Category"
                content={category}
            >
                </DetailsSection>
            <DetailsSection
                className="ProposalDetails__description"
                name="Description"
                content={description}
            >
            </DetailsSection>
        </div>
    );
};

export default ProposalDetails;
