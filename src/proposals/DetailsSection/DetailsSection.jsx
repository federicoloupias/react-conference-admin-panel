import React from "react";
import classNames from "classnames";

import "./DetailsSection.css";

const DetailsSection = ({ className, name, content }) => (
    <section className={classNames("DetailsSection", className)}>
        <div className="DetailsSection__name">
            {name}
        </div>
        <div className="DetailsSection__content">
            <span className={`ProposalDetails__${name}__value`}>
                    {content}
            </span>
        </div>
    </section>
);

export default DetailsSection;
